\context ChordNames 
	\chords {
		\set chordChanges = ##t
		% intro
		c2. e2.:m f2. g2.

		% santo es...
		c2. e2.:m f2. g2.
		c2. e2.:m f2. g2.

		% los cielos...
		e2.:m a2.:m d2. g2.

		% hosanna...
		c2. g2. a2.:m e2.:m
		f2. g2. f2. g2.

		% bendito...
		e2.:m a2.:m d2. g2.

		% hosanna...
		c2. g2. a2.:m e2.:m
		f2. g2. f2. g2.
		c2. c2.
	}
