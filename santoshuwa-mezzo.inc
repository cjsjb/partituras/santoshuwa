\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "treble"
		\key c \major

		R2.*4  |
%% 5
		c' 4 g' 8 g' 4.  |
		b 4 g' 8 g' 4.  |
		r8 r f' f' 4 e' 8  |
		e' 4 ( d' 8 ) c' 4 ( d' 8 )  |
		c' 4 g' 8 g' 4.  |
%% 10
		b 4 g' 8 g' 4.  |
		r8 f' f' f' 4 e' 8  |
		e' 4 ( d' 8 ) c' 4 d' 8  |
		e' 4 e' e' 8 d'  |
		c' 4 c' c' 8 c'  |
%% 15
		d' 4 d' d' 8 c'  |
		b 4 ( c' 8 ) d' 4 r8  |
		c' 2.  |
		b 4. b  |
		c' 2.  |
%% 20
		b 4. b 8. r16 b 8  |
		e' 4 e' e' 8 e'  |
		e' 4 ( d' 8 ) b 8. r16 b 8  |
		e' 4 e' e' 8 e'  |
		e' 4 ( d' 8 ) b 8. r16 f' 8  |
%% 25
		e' 4 e' e' 8 d'  |
		c' 4 c' 8. r16 c' 8 c'  |
		d' 4 d' d' 8 c'  |
		b 4 ( c' 8 d' 4 ) r8  |
		c' 2.  |
%% 30
		b 4. b  |
		c' 2.  |
		b 4. b 8. r16 b 8  |
		e' 4 e' e' 8 e'  |
		e' 4 ( d' 8 ) b 8. r16 b 8  |
%% 35
		e' 8. e' 4 r16 e' 8 e'  |
		e' 4 ( d' 8 c' 4 d' 8 )  |
		e' 2.  |
		R2.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		San -- to es el Se -- ñor
		Dios po -- de -- ro -- so. __
		San -- to es el Se -- ñor
		Dios del u -- ni ver -- so.

		Los cie -- los y la tie -- rra
		es -- tán lle -- nos de tu glo -- ria.

		¡Ho -- san -- na!
		¡Ho -- san -- na!
		¡Ho -- san -- na en el cie -- lo!
		¡Ho -- san -- na en el cie -- lo!

		Ben -- di -- "to es" el que vie -- ne
		en el nom -- bre del Se -- ñor. __

		¡Ho -- san -- na!
		¡Ho -- san -- na!
		¡Ho -- san -- na en el cie -- lo!
		¡Ho -- san -- na en el cie -- lo!
	}
>>
